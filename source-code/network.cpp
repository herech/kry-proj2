/*
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt do předmětu KRY, program naváže SSL spojení s danými doménami a validuje předložené certifikáty, výsledky poté shrne do souboru.
 */

#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <string>
#include <errno.h>
#include <strings.h>
#include <iostream>

#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <openssl/crypto.h>
#include <openssl/x509.h>

#include "network.hpp"
#include "openssl_hostname_validation.hpp"

using namespace std;

// definujme konstanty pro míru úrovně důvěry ve stránku která předložila daný certifikát
#define LEVEL_OF_TRUST_UNTRUSTWORTHY 4
#define LEVEL_OF_TRUST_SUSPICIOUS 3
#define LEVEL_OF_TRUST_TRUSTWORTHY 2
#define LEVEL_OF_TRUST_TRUSTWORTHY_AND_SECURE 1

/**
 * Převzato z http://www.zedwood.com/article/c-openssl-parse-x509-certificate-pem
 * Funkce získá název algoritmu podpisu certifikátu
 */
string getSignatureAlgorithm(X509 *x509) {
    int sig_nid = OBJ_obj2nid((x509)->sig_alg->algorithm);
    return string( OBJ_nid2ln(sig_nid) );
}

/**
 * Převzato z http://www.zedwood.com/article/c-openssl-parse-x509-certificate-pem
 * Funkce získá název algoritmu veřejného klíče certifikátu
 */
string getPublicKeyType(X509 *x509) {
    EVP_PKEY *pkey=X509_get_pubkey(x509);
    int key_type = EVP_PKEY_type(pkey->type);
    EVP_PKEY_free(pkey);
    if (key_type==EVP_PKEY_RSA) return "rsa";
    if (key_type==EVP_PKEY_DSA) return "dsa";
    if (key_type==EVP_PKEY_DH)  return "dh";
    if (key_type==EVP_PKEY_EC)  return "ecc";
    return "";
}

/**
 * Převzato z http://www.zedwood.com/article/c-openssl-parse-x509-certificate-pem
 * Funkce získá velikost veřejného klíče certifikátu
 */
int getPublicKeySize(X509 *x509) {
    EVP_PKEY *pkey=X509_get_pubkey(x509);
    int key_type = EVP_PKEY_type(pkey->type);
    int keysize = -1; //or in bytes, RSA_size() DSA_size(), DH_size(), ECDSA_size();
    keysize = key_type==EVP_PKEY_RSA && pkey->pkey.rsa->n ? BN_num_bits(pkey->pkey.rsa->n) : keysize;
    keysize = key_type==EVP_PKEY_DSA && pkey->pkey.dsa->p ? BN_num_bits(pkey->pkey.dsa->p) : keysize;
    keysize = key_type==EVP_PKEY_DH  && pkey->pkey.dh->p  ? BN_num_bits(pkey->pkey.dh->p) : keysize;
    keysize = key_type==EVP_PKEY_EC  ? EC_GROUP_get_degree(EC_KEY_get0_group(pkey->pkey.ec)) : keysize;
    EVP_PKEY_free(pkey);
    return keysize;
}

/**
 * Funkce uvolní použité objekty bio, ctx a certifikát serveru 
 */
void freeAlocatedMemory(BIO * bio, SSL_CTX * ctx, X509* server_cert) {
    if (bio != NULL) BIO_free_all(bio);
    if (ctx != NULL) SSL_CTX_free(ctx);
    if (server_cert != NULL) X509_free(server_cert);
}

/**
 * Funkce vytvoří SSL spojení s danou doménou a validuje předložený certifikát
 * @param domainName název domény se kterou se bude vytvářet SSL spojení
 * @param certificateValidationResult řetezec obsahující slovní popis výsledku validce předloženého certifikátu
 * @param levelOfTrust stupeň důvěry ve stránku na základě předloženého certifikátu (1 - největší, 4 - nejmenší)
 * @return true - pokud došlo k fatální chybě a je třeba ukončit program, jinak false
 */
bool createSSLConnectionToHostnameAndValidateCertificate(string domainName, string* certificateValidationResult, int* levelOfTrust) {

    // Převzato z https://www.ibm.com/developerworks/library/l-openssl/
    //            https://github.com/openssl/openssl/blob/master/include/openssl/x509_vfy.h
    //            https://wiki.openssl.org/index.php/SSL/TLS_Client

    //inicializujeme OpenSSL
    SSL_library_init();
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();

    // vytvoříme základní pomocné struktury
    BIO * bio;
    SSL_CTX * ctx = SSL_CTX_new(SSLv23_client_method());
    if (ctx == NULL) {
        fprintf(stderr, "\nChyba: nepodařilo se inicliazizovat strukturu ctx.\n");
        return true;
    }
    SSL * ssl;

    // načteme lokální CA, které věříme my
    if (SSL_CTX_load_verify_locations(ctx, "crocs-ca.pem", NULL) <= 0) {
        fprintf(stderr, "\nChyba: nepodařilo se načíst certifikát ze souboru crocs-ca.pem\n");
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        freeAlocatedMemory(NULL, ctx, NULL);
        return true;
    }
    
    // načteme CA, kterým věří merlin
    if (SSL_CTX_load_verify_locations(ctx, "/etc/pki/tls/cert.pem", NULL) <= 0) {
        fprintf(stderr, "\nChyba: nepodařilo se načíst certifikáty ze souboru /etc/pki/tls/cert.pem\n");
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        freeAlocatedMemory(NULL, ctx, NULL);
        return true;
    }

    // inicializujeme pomocné struktury
    bio = BIO_new_ssl_connect(ctx);
    if (bio == NULL) {
        fprintf(stderr, "\nChyba: nepodařilo se inicliazizovat strukturu bio.\n");
        freeAlocatedMemory(NULL, ctx, NULL);
        return true;
    }
    // získáme ukazatel na SSL ze struktury BIO
    BIO_get_ssl(bio, &ssl);
    if (ssl == NULL) {
        fprintf(stderr, "\nChyba: nepodařilo se inicliazizovat strukturu ssl.\n");
        freeAlocatedMemory(bio, ctx, NULL);
        return true;
    }
    // nový handshake si bude řešit openssl na pozadí samo
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
    
    // zvolíme si požadovanou virtuální doménu
    SSL_set_tlsext_host_name(ssl, domainName.c_str());
    
    // pokusíme se připojit k serveru
    BIO_set_conn_hostname(bio, (domainName + string(":") + string("443")).c_str());

    // ověříme jestli se vytvořilo zabezpečené spojení, pokud ne, tak není důvěra ke stránce žádná
    if(BIO_do_connect(bio) <= 0) {
        *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY;
        *certificateValidationResult = string("Error establishing SSL connection. Probably fatal problems with the certificate. Detail: ") + string(ERR_reason_error_string(ERR_get_error()));
        freeAlocatedMemory(bio, ctx, NULL);
        return false;
    }
    
    // ověříme jestli se vytvořilo zabezpečené spojení (asi není kvůli BIO_do_connect potřeba, ale pro jistotu), pokud ne, tak není důvěra ke stránce žádná
    if(BIO_do_handshake(bio) <= 0) {
        *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY;
        *certificateValidationResult = string("Error establishing SSL connection. Probably fatal problems with the certificate. Detail: ") + string(ERR_reason_error_string(ERR_get_error()));
        freeAlocatedMemory(bio, ctx, NULL);
        return false;
    }
    
    // získáme certifikát serveru
    X509* server_cert = SSL_get_peer_certificate(ssl);
    // pokud server neposkytnul certifikát, není pro nás vůbec důvěryhodný
    if(NULL == server_cert) {
        *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY;
        *certificateValidationResult = string("Error, server didn't provide certificate");
        freeAlocatedMemory(bio, ctx, NULL);
        return false;
    }

    // zíksáme výsledek validace ceritifikátu
    int resultOfCertificateVerification = SSL_get_verify_result(ssl); 
    
    // pokud není certifikát validní
    if(resultOfCertificateVerification != X509_V_OK) {
        *levelOfTrust = LEVEL_OF_TRUST_SUSPICIOUS; // pokud neprojde validace certifikátu, tak výchozí úroveň důvěry je 3
        *certificateValidationResult = string("Error (with code ") + string(to_string(resultOfCertificateVerification)) + string(") during validation certificate. ") + string("Detail: ") + string(X509_verify_cert_error_string(resultOfCertificateVerification));
        
        // v případě vážnějšího problému nastavíme případně úroveň důvěry na 4, jinak necháme na 3
        switch(resultOfCertificateVerification) {
            case X509_V_ERR_CERT_HAS_EXPIRED:                   // code 10, ponecháme 3
                break;
            case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:        // code 18, ponecháme 3
                break;
            case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:          // code 19, ponecháme 3
                break;
            case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:  // code 20, ponecháme 3
                break;
            case X509_V_ERR_INVALID_CA:                         // code 24, nvážnější problém nastavíme na 4
            case X509_V_ERR_PERMITTED_VIOLATION:                // code 47, vážnější problém, nastavíem na 4
                *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY;
                break;
        }
        
        // provedeme ještě kontrolu hostname v certidikátu serveru, server by nám takto snadno mohl podvrhnitu certifikát, který je určen pro jinou doménu, tohle by byl vážný prohřešek, který by přebyl i případné předchozí problémy
        HostnameValidationResult hostnameValidationResult = validate_hostname(domainName.c_str(), server_cert);
        // kontrola na doménové jméno v certifikátu serveru se z nějakého důvodu nezdařila, to je velmi špatný, bijte na poplach
        if(hostnameValidationResult != MatchFound) {
            *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY; 
            *certificateValidationResult = string("Error during validation hostname in server certificate.");
        }
    }
    // certifikát prošel základní kontrolou, budeme provádět další kontroly
    else {
        // první z navazujících kontrol bude kontrola hostname v certidikátu serveru, server by nám takto snadno mohl podvrhnitu certifikát, který je určen pro jinou doménu, tohle by byl vážný prohřešek
        HostnameValidationResult hostnameValidationResult = validate_hostname(domainName.c_str(), server_cert);
        // kontrola na doménové jméno v certifikátu serveru se z nějakého důvodu nezdařila, to je velmi špatný, bijte na poplach
        if(hostnameValidationResult != MatchFound) {
            *levelOfTrust = LEVEL_OF_TRUST_UNTRUSTWORTHY; 
            *certificateValidationResult = string("Error during validation hostname in server certificate.");
            freeAlocatedMemory(bio, ctx, server_cert);
            return false;    
        }
        
        // kontrola jak moc je použit silný algoritmus veřejného klíče
        // řídíme se tabulkou https://wiki.openssl.org/index.php/Elliptic_Curve_Cryptography
        string typeOfPublicKey = getPublicKeyType(server_cert); // typ algoritmu veřejného klíče
        int sizeOfPublicKey = getPublicKeySize(server_cert);    // velikost veřejného klíče v bitech
        
        if (typeOfPublicKey == "rsa" && sizeOfPublicKey < 2048) {
            *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY;
            *certificateValidationResult = string("No error, but not very secure. Certificate uses public key algorithm RSA with less than 2048b key."); 
            freeAlocatedMemory(bio, ctx, server_cert);
            return false; 
        } 
        else if (typeOfPublicKey == "dsa" && sizeOfPublicKey < 2048) {
            *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY;
            *certificateValidationResult = string("No error, but not very secure. Certificate uses public key algorithm DSA with less than 2048b key."); 
            freeAlocatedMemory(bio, ctx, server_cert);
            return false; 
        } 
        else if (typeOfPublicKey == "dh" && sizeOfPublicKey < 2048) {
            *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY;
            *certificateValidationResult = string("No error, but not very secure. Certificate uses public key algorithm DH with less than 2048b key."); 
            freeAlocatedMemory(bio, ctx, server_cert);
            return false; 
        } 
        
        else if (typeOfPublicKey == "ecc" && sizeOfPublicKey < 224) {
            *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY;
            *certificateValidationResult = string("No error, but not very secure. Certificate uses public key algorithm ECC with less than 224b key."); 
            freeAlocatedMemory(bio, ctx, server_cert);
            return false; 
        } 
        
        // ověření síly algoritmu podpisu
        // řídíme se tabulkou https://en.wikipedia.org/wiki/Secure_Hash_Algorithms
        string signatureAlgorithm = getSignatureAlgorithm(server_cert);
        if (signatureAlgorithm == "sha1WithRSAEncryption" || signatureAlgorithm == "sha0WithRSAEncryption" || signatureAlgorithm == "md5WithRSAEncryption") {
            *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY;
            *certificateValidationResult = string("No error, but not very secure. Certificate uses signature algorithm ") + signatureAlgorithm + string(", which is not considered safe."); 
            freeAlocatedMemory(bio, ctx, server_cert);
            return false; 
        }
        
        // pokud certifikát přežil všechny kontroly, je považován za maximálně bezpečný         
        *levelOfTrust = LEVEL_OF_TRUST_TRUSTWORTHY_AND_SECURE;
        *certificateValidationResult = string("All right. Great security provided."); 
        
        // Převzato z http://www.zedwood.com/article/cpp-libssl-get-peer-certificate
        // uložíme si při debugovani certifikát serveru
        /*BIO * bio_out = BIO_new_file((string("output/") + domainName + string(".pem")).c_str(), "w");
        if (bio_out)
        {
            X509_print(bio_out, server_cert);//parsed
            PEM_write_bio_X509(bio_out, server_cert);
            BIO_free(bio_out);
        } */
        
    }
    
    // uvolníme naalokovaný prostor
    freeAlocatedMemory(bio, ctx, server_cert);
    
    return false;

}
