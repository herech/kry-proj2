/*
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt do předmětu KRY, program naváže SSL spojení s danými doménami a validuje předložené certifikáty, výsledky poté shrne do souboru.
 */

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "network.hpp"

using namespace std;

/**
 * Klasicka funkce main, která iteruje přes domény a zkouší s nimi navázat SSL spojení a následně validovat předložené certifikáty
 * @param argc počet vstupních argumentů
 * @param argv pole vstupních argumentů
 * @return 0, nebo 1 v případě chyby
 */
int main(int argc, char **argv)
{
    ofstream myfile;                                        // výstupní soubor
    myfile.open ("xherec00-domains.csv");                   // otevřeme výstupní soubor pro zápis
    if (!myfile) {
        cerr << "Error. Cannot open file xherec00-domains.csv for write.\n";
        return 1;
    }
    
    string domainName;                                      // název domény, ze které budeme získávat certifikát, skládá se ze statické a dynamické složky
    string staticPartOfDomainName = ".minotaur.fi.muni.cz"; // neměná část doménového jména
    char dynamicPartOfDomainName[3];                        // dvoumístné číslo (které je variabilní v rámci domény) domény 5. řádu + \0
    string certificateValidationResult;                     // slovní popis výsledku validace ceritfikátu
    int levelOfTrust;                                       // stupeň důvěry v certifikát
    bool fatalError;                                        // nastala vážná chyba, kvůli které je třeba ukončit program?
    
    // postupně pro domény 00.minotaur.fi.muni.cz až 99.minotaur.fi.muni.cz navážeme SSL spojení
    // zároveň validujeme předložený certifikát a výsledky validace vypíšeme jako tabulku do csv souboru
    for (int i = 0; i <= 99;i++) {
        // převedení čísla variabilní části domény na řetězec
        sprintf (dynamicPartOfDomainName, "%.2d", i); 
        // sločením dynamické a statické části domény vytvoříme úplný název domény se kterou budeme navazovat SSl spojení
        domainName = string(dynamicPartOfDomainName) + staticPartOfDomainName; 
        // vytvoření SSL spojení s danou doménou a validace předloženého certifikátu
        fatalError = createSSLConnectionToHostnameAndValidateCertificate(domainName, &certificateValidationResult, &levelOfTrust);
        // pokud došlo k fatální chybě, ukončíme program
        if (fatalError == true) return 1;
        
        // výpis výsledků validace certifikátu na obrazovku při debugování
        // cout << domainName << " | " << levelOfTrust << " | " << certificateValidationResult << endl;
        
        // zápis výsledků validace certifikátu do .csv souboru, certificateValidationResult pro jistotu obalíme uvozovkama, může obsahovat totiž čárku
        myfile << domainName << ", " << levelOfTrust << ", " << "\"" << certificateValidationResult << "\"";
        // za posledním výsledkem už není další řádek
        if (i < 99) {
            myfile << endl;
        }
        if (!myfile) {
            cerr << "Error. Cannot write to file domains-output.csv.\n";
            return 1;
        }
    }
    
    myfile.close();
    
    return 0;
            
}
