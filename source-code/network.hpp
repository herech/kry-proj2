/*
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt do předmětu KRY, program naváže SSL spojení s danými doménami a validuje předložené certifikáty, výsledky poté shrne do souboru.
 */
 
#ifndef NETWORK_HPP
#define NETWORK_HPP

#include <string>

using namespace std;

/**
 * Funkce vytvoří SSL spojení s danou doménou a validuje předložený certifikát
 * @param domainName název domény se kterou se bude vytvářet SSL spojení
 * @param certificateValidationResult řetezec obsahující slovní popis výsledku validce předloženého certifikátu
 * @param levelOfTrust stupeň důvěry ve stránku na základě předloženého certifikátu (1 - největší, 4 - nejmenší)
 * @return true - pokud došlo k fatální chybě a je třeba ukončit program, jinak false
 */
bool createSSLConnectionToHostnameAndValidateCertificate(string domainName, string* certificateValidationResult, int* levelOfTrust);


#endif /* NETWORK_HPP */
